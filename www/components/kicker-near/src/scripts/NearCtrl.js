angular.module('robinboehm.kickerNear')
  .controller('NearCtrl', function ($scope, $ionicPopup, KickerData, geolocation) {


    $scope.$on('$ionicView.beforeEnter', init);
    $scope.options = {
      radius: 20
    };

    function init() {
      geolocation.getLocation()
        .then(function (pos) {
          $scope.options.latitude = pos.coords.latitude;
          $scope.options.longitude = pos.coords.longitude;
          $scope.search();
        }, function (errorMessage) {
            $ionicPopup.alert({
              title: 'GPS Location Service',
              template: errorMessage
            });
        });
    }


    $scope.search = function () {
      KickerData.near($scope.options)
        .then(function (locations) {
          $scope.locations = locations;
        });
    };


  });