#import "SnapshotHelper.js"

var target = UIATarget.localTarget();
var app = target.frontMostApp();
var window = app.mainWindow();


target.delay(1)

var target = UIATarget.localTarget();

app.mainWindow().scrollViews()[1].webViews()[0].textFields()["E-Mail Adresse"].tap();
app.keyboard().typeString("karl@leichtr.de");
app.mainWindow().scrollViews()[1].webViews()[0].secureTextFields()["Passwort"].tap();
app.keyboard().typeString("peng123!\n");

captureLocalizedScreenshot("0-Dashboard")


target.frontMostApp().mainWindow().scrollViews()[1].webViews()[0].staticTexts()["Paula"].tap();
target.delay(1)
captureLocalizedScreenshot("1-DetailView")


target.frontMostApp().mainWindow().scrollViews()[1].webViews()[0].tapWithOptions({tapOffset:{x:0.85, y:0.98}});
target.frontMostApp().mainWindow().scrollViews()[1].webViews()[0].textFields()["Wobei benötigen Sie Hilfe?"].tap();
target.frontMostApp().keyboard().typeString("Medikamente besorgen");
target.frontMostApp().mainWindow().scrollViews()[1].webViews()[0].textFields()["Weitere Details(ggf. Gegenleistung)"].tap();
target.frontMostApp().keyboard().typeString("Wer kann mir meine Medikamente aus der Apotheke mitbringen?");
target.frontMostApp().mainWindow().scrollViews()[1].webViews()[0].staticTexts()[0].tapWithOptions({tapOffset:{x:0.69, y:0.79}});
target.frontMostApp().statusBar().tapWithOptions({tapOffset:{x:0.75, y:0.70}});
target.frontMostApp().mainWindow().scrollViews()[1].webViews()[0].textFields()["Wobei benötigen Sie Hilfe?"].scrollToVisible();

target.delay(1)
captureLocalizedScreenshot("2-CreateTask")
