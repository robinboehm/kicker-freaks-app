angular.module('robinboehm.kickerMap')
  .controller('MapCtrl', function ($scope,$timeout, $ionicLoading, KickerData) {
    KickerData.getAll($scope.options)
      .then(function (locations) {
        $scope.map.markers = locations;
      });

    $scope.map = {
      control: {},
      markerIcon: 'img/kicker-marker.png',
      center: {
        latitude: $stateParams.latitude || 51.456680,
        longitude: $stateParams.longitude || 7.009991
      },
      options: {scrollwheel: false},
      zoom: 16,
      models: []
    }
  });