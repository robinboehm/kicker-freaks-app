angular.module('robinboehm.kickerData')
  .factory('KickerData', function ($http) {

    var apiLocation = 'http://kicker-freaks.com/api/locations/';
    //var apiLocation = 'http://localhost:3000/api/locations/';

    // Public API
    return {
      getAll: function () {
        return _getAll()
      },
      getById: function (id, params) {
        return _get(id, params);
      },
      getCheckIns: function (id) {
        return _getCheckIns(id);
      },
      checkIn: function (id) {
        return _checkIn(id);
      },
      checkOut: function(locationId, checkInId){
        return _checkOut(locationId,checkInId)
      },
      announceCheckIn: function(id){
        return _announceCheckIn(id)
      },
      update: function (location) {
        return _update(location);
      },
      create: function (location) {
        return _create(location);
      },
      delete: function (location) {
        return _delete(location);
      },
      /**
       *
       * @param options
       *  default: { longitude=51.456680, latitude=7.009991, radius=5 }
       * @returns {*}
       */
      near: function (options) {
        return _near(options);
      }
    };

    function extractData(response) {
      return response.data;
    }

    function _getAll() {
      return $http.get(apiLocation)
        .then(extractData);
    }

    function _get(id, params) {
      return $http.get(apiLocation + id, {
        params: params
      })
        .then(extractData);
    }

    function _getCheckIns(id) {
      return $http.get(apiLocation + id + '/check_ins')
        .then(extractData);
    }

    function _checkIn(id) {
      return $http.get(apiLocation + id + '/check_ins/new')
        .then(extractData);
    }

    function _checkOut(id, checkInId) {
      return $http.delete(apiLocation + id + '/check_ins/' + checkInId)
        .then(extractData);
    }

    function _announceCheckIn(id) {
      return $http.post(apiLocation + id + '/check_ins/later')
        .then(extractData);
    }

    function _update(location) {
      return $http.put(apiLocation + location.id, location)
        .then(extractData)
    }

    function _create(location) {
      return $http.post(apiLocation, location)
        .then(extractData)
    }

    function _delete(location) {
      return $http.delete(apiLocation + location.id)
        .then(extractData);
    }


    function _near(options) {

      // set default
      options = options ? options : {};
      var longitude = options.longitude ? options.longitude : 7.009991;
      var latitude = options.latitude ? options.latitude : 51.456680;
      var radius = options.radius ? options.radius : 5;


      return $http.get(apiLocation, {
        params: {
          longitude: longitude,
          latitude: latitude,
          radius: radius
        }
      })
        .then(extractData);
    }
  });