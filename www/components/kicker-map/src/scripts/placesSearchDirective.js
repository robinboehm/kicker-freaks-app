angular.module('robinboehm.kickerMap')
  .directive('placesSearch', function () {
    return {
      link: function (scope, element) {

        var mapInitialized = false;
        scope.$watch('map', function(mapObject){
          if(!mapInitialized && angular.isObject(mapObject)){
            mapInitialized = true;
            var input = element;
            scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var searchBox = new google.maps.places.SearchBox(input);

            // Listen for the event fired when the user selects an item from the
            // pick list. Retrieve the matching places for that item.
            var markers = [];

            google.maps.event.addListener(searchBox, 'places_changed', function () {
              var places = searchBox.getPlaces();

              if (places.length == 0) {
                return;
              }
              for (var i = 0, marker; marker = markers[i]; i++) {
                marker.setMap(null);
              }

              // For each place, get the icon, place name, and location.
              markers = [];
              var bounds = new google.maps.LatLngBounds();
              for (var i = 0, place; place = places[i]; i++) {
                var image = {
                  url: place.icon,
                  size: new google.maps.Size(71, 71),
                  origin: new google.maps.Point(0, 0),
                  anchor: new google.maps.Point(17, 34),
                  scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                var marker = new google.maps.Marker({
                  map: scope.map,
                  icon: image,
                  title: place.name,
                  position: place.geometry.location
                });

                markers.push(marker);

                bounds.extend(place.geometry.location);
              }

              scope.map.fitBounds(bounds);
              scope.map.setZoom(14);
            });


            // Bias the SearchBox results towards places that are within the bounds of the
            // current map's viewport.
            google.maps.event.addListener(scope.map, 'bounds_changed', function () {
              var bounds = scope.map.getBounds();
              searchBox.setBounds(bounds);
            });
          }
        });



      }
    }
  });