angular.module('robinboehm.kickerMap')
  .controller('MapControlCtrl', function ($scope, $timeout, $ionicLoading, KickerData) {


    $scope.center = function () {
      navigator.geolocation.getCurrentPosition(function (pos) {
        $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
        $scope.map.setZoom(14);
      }, function (error) {
        alert('Unable to get location: ' + error.message);
      });
    }
  });