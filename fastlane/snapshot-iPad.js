#import "SnapshotHelper.js"

var target = UIATarget.localTarget();
var app = target.frontMostApp();
var window = app.mainWindow();
var webView = app.mainWindow().scrollViews()[1].webViews()[0];
target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_LANDSCAPELEFT);

target.delay(1)

webView.textFields()["E-Mail Adresse"].tap();
app.keyboard().typeString("karl@leichtr.de");
webView.secureTextFields()["Passwort"].tap();
app.keyboard().typeString("peng123!\n");

captureLocalizedScreenshot("0-Dashboard")


webView.staticTexts()["Paula"].tap();
target.delay(1)
captureLocalizedScreenshot("1-DetailView")


webView.tapWithOptions({tapOffset:{x:0.85, y:0.98}});
webView.textFields()["Wobei benötigen Sie Hilfe?"].tap();
target.frontMostApp().keyboard().typeString("Medikamente besorgen");
webView.textFields()["Weitere Details(ggf. Gegenleistung)"].tap();
target.frontMostApp().keyboard().typeString("Wer kann mir meine Medikamente aus der Apotheke mitbringen?");
webView.staticTexts()[0].tapWithOptions({tapOffset:{x:0.69, y:0.79}});
target.frontMostApp().statusBar().tapWithOptions({tapOffset:{x:0.75, y:0.70}});
webView.textFields()["Wobei benötigen Sie Hilfe?"].scrollToVisible();


target.delay(1)
captureLocalizedScreenshot("2-CreateTask")
