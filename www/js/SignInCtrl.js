angular.module('starter')
  .controller('SignInCtrl', function ($scope, $http, $state) {

    $scope.signIn = function () {
      $http.post('http://localhost:3000/api/sessions', undefined, {
          params: {
            email: 'robinboehm@googlemail.com',
            password: '12345678'
          }
        }
      ).then(function (response) {
          var token = response.data.token;
          console.log(token);
          $http.defaults.headers.common.Authorization = token;
          $state.go('tab.near');
        });
    };
  });