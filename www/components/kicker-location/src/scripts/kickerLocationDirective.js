angular.module('robinboehm.kickerForm')
  .directive('kickerLocation', function () {

    return {
      restrict: 'E',
      templateUrl: 'components/kicker-location/src/templates/kicker-location.html'
    }
  });