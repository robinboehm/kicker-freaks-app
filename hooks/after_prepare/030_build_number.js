#!/usr/bin/env node

// Based on the 'replace text' hook found here: http://devgirl.org/2013/11/12/three-hooks-your-cordovaphonegap-project-needs/
// This hook should be placed in the 'after_prepare' hook folder.

// The hook relies on a JSON file located at '<project_root>/resources/.build.json' to track the build number.
// build.json content:
// {"build: 1}

// Add 'BUILDNR' to the version number in the '<project_root>/config.xml' file.

// this plugin replaces arbitrary text in arbitrary files
//
// Look for the string CONFIGURE HERE for areas that need configuration
//

var fs = require('fs'),
  path = require('path'),
  exec = require('child_process').exec,
  xml2js = require('xml2js');

var rootdir = process.argv[2],
  current_build_number = 0;

// Use the output from `git rev-list --all |wc -l` (example: 171) for the current build number.
exec("git rev-list --all |wc -l ", process_output);

function process_output(error, stdout, stderr) {
  current_build_number = parseInt(stdout);
  update_build_number(current_build_number);
}

function replace_string_in_file(filename, to_replace, replace_with) {
  var data = fs.readFileSync(filename, 'utf8');
  var result = data.replace(new RegExp(to_replace, "g"), replace_with);
  fs.writeFileSync(filename, result, 'utf8');
}

var target = "stage";
if (process.env.TARGET) { target = process.env.TARGET; }

function update_build_number(build_number){
  if (rootdir) {
    var parser = new xml2js.Parser();
    fs.readFile('./config.xml', function(err, data) {
      if(err) { throw(err); }
      parser.parseString(data, function (err, result) {
        applicationName = result.widget['name'][0];
        var buildstoreplace = [
          //ios
          "platforms/ios/" + applicationName + "/config.xml",
          "platforms/ios/" + applicationName + "/" + applicationName + "-Info.plist",

          //android
          "platforms/android/AndroidManifest.xml",
          "platforms/android/ant-build/AndroidManifest.cordova.xml",
          "platforms/android/res/xml/config.xml"
        ];

        buildstoreplace.forEach(function(val, index, array) {
          "use strict";
          var fullfilename = path.join(rootdir, val);

          if (fs.existsSync(fullfilename)) {
            replace_string_in_file(fullfilename, "BUILDNR", build_number);
          } else {
            //console.log("missing: "+fullfilename);
          }
        });
        console.log('Build number updated to ' + build_number);
      });
    });
  }
}
