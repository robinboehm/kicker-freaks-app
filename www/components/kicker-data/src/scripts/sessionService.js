angular.module('robinboehm.kickerData')
  .factory('sessionService', function($http, $q, sessionApiService) {
    var userIsLoggedIn = false;

    function loginWithToken(token) {
      if (token) {
        userIsLoggedIn = true;
        localStorage.setItem('Authorization', token);
      } else {
        userIsLoggedIn = false;
        delete window.localStorage['Authorization'];
      }
      $http.defaults.headers.common['Authorization'] = token;
    }

    function loginWithCredentials(credentials) {
      return sessionApiService.create(credentials).then(
        function(session) {
          loginWithToken(session.token);
        },
        function(errorResponse) {
          if (errorResponse.status === 401) {
            loginWithToken();
            return $q.reject('login_failed');
          }
        });
    }

    function logout() {
      loginWithToken();
      return $q.when('logged out');
    }

    function isLoggedIn() {
      return userIsLoggedIn || !!localStorage['Authorization'];
    }

    return {
      loginWithToken: loginWithToken,
      loginWithCredentials: loginWithCredentials,
      logout: logout,
      isLoggedIn: isLoggedIn
    };
  });