angular.module('robinboehm.kickerLocation')
  .controller('ShowLocationCtrl', function ($scope, $stateParams, $ionicPopup, KickerData, geolocation) {

    var options = {
      radius: 1
    };

    // quick, dirty, too much calls, yeah plz optimize
    var getLocation = function () {
      return KickerData.getById($stateParams.locationId)
        .then(function (location) {
          $scope.location = location;
          return geolocation.getLocation();
        })
        .then(function (pos) {
          return KickerData.getById($stateParams.locationId, {
            latitude: pos.coords.latitude,
            longitude: pos.coords.longitude
          })
        }, function (errorMessage) {
          $ionicPopup.alert({
            title: 'GPS Location Service',
            template: errorMessage
          });
        })
        .then(function (location) {
          $scope.location = location;
        });
    };

    $scope.checkIn = function () {
      KickerData.checkIn($stateParams.locationId)
        .then(getLocation);
    };

    $scope.checkOut = function (locationId, checkInId) {
      KickerData.checkOut(locationId, checkInId)
        .then(getLocation);
    };

    $scope.announceCheckIn = function (locationId) {
      KickerData.announceCheckIn(locationId)
        .then(getLocation);
    };


    // Init
    getLocation();


    $scope.navigateTo = function () {
      launchnavigator.navigate([$scope.location.latitude, $scope.location.longitude]);
    }


  });