angular.module('robinboehm.kickerTournament')
  .directive('tournamentList',function(KickerData, geolocation){
    return {
      restrict: 'E',
      templateUrl: 'components/kicker-tournament/components/tournament-list/tournament-list.component.html',
      scope: {},
      controller: tournamentListVM,
      controllerAs: 'tournamentList'
    };


    function tournamentListVM(){
      this.locations = [
        {name: 'Kixx Einzelturnierserie', location: 'Kixx Hamburg', date: '09.01.2016 20:30 Uhr'},
        {name: 'DTFB-Challenger - TFBS Koblenz e.V.', location: 'BBW Heinrich-Haus', date: '16.01.2016 11:30 Uhr'},
        {name: 'Neujahrs Open 2016 Oldenburg', location: '3RaumWohnung', date: '09.01.2016 12:00 Uhr'},
        {name: 'Mini-Challenger (Kixx Mittwochsdoppel)', location: 'Kixx Hamburg', date: '06.01.2016 11:30 Uhr'}
      ];

      var options = {
        radius: 100
      };

      function init() {
        geolocation.getLocation()
          .then(function (pos) {
            options.latitude = pos.coords.latitude;
            options.longitude = pos.coords.longitude;
            this.search();
          }, function (errorMessage) {
            $ionicPopup.alert({
              title: 'GPS Location Service',
              template: errorMessage
            });
          });
      }


      this.search = function () {
        KickerData.near(options)
          .then(function (locations) {
            this.locations = locations;
          });
      };




    }

  });