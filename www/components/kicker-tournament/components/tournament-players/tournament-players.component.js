angular.module('robinboehm.kickerTournament')
  .directive('tournamentPlayers', function () {
    return {
      restrict     : 'E',
      templateUrl : 'components/kicker-tournament/components/tournament-players/tournament-players.component.html',
      scope       : {},
      controller  : tournamentPlayersVM,
      controllerAs : 'tournamentPlayers'
    };


    function tournamentPlayersVM () {
      // GoTo http://tifu.info/turnier_voranmeldungen.php?turnierid=5737
      // Console: JSON.stringify(Array.prototype.slice.call(document.getElementsByClassName('textlinks')).map((element)=>{return {name:element.textContent}}))
      this.players = [{"name" : "Jessica Hellmich / Manuel Stahl"}, {"name" : "Christoph Hardt / N.N."}, {"name" : "Azat Anwar / Yves Junker"}, {"name" : "Jessica Bechtel / Benjamin Struth"}, {"name" : "Marc Stoffel / Salvatore LoManto"}, {"name" : "Bernhard Rieden / Philip Passin"}, {"name" : "Mario Urmitzer / Christian Banyai"}, {"name" : "Julian Wortmann / Torsten Grünkorn"}, {"name" : "Marco Eichel / Thomas Steffens"}, {"name" : "Frank Stoffel / Ralf Kowalik"}, {"name" : "Dennis Schwenzer / Jeremy Moeller"}, {"name" : "Jan-Michael Nikenich / Daniel Sorger"}, {"name" : "Pascal Pütz / Ryan Merrifield"}, {"name" : "Thomas Müller / Hasan Genc"}, {"name" : "Andreas Igel / Sven Igel"}, {"name" : "Justin Schmidt Duarte / Alexander Pütz"}, {"name" : "Thomas Blum / Heinrich Schmidt"}, {"name" : "Jörg Lippold / Klaus Daunicht"}, {"name" : "Andreas Stamm / N.N."}, {"name" : "Raffael Herzer / Simon Mai"}, {"name" : "Alessandro De Micco / Dirk Mattesen"}, {"name" : "Manfred Blaskowitz / N.N."}, {"name" : "Matthias Töller / Michael Linke"}, {"name" : "Leonie Linke / N.N."}, {"name" : "Levente Virag / Hunor Venczel"}, {"name" : "Tim Pietzker / Dominic Voß"}, {"name" : "Silas Meister / Karsten Kuckhoff"}, {"name" : "Marvin Gabler / Tim Pauly"}, {"name" : "Gerd Kratz / Philipp Bormann"}, {"name" : "Vanessa Heisel / N.N."}, {"name" : "Astrid Lang / Sarah Klabunde"}, {"name" : "Dominik Pfingst / André Stockmanns"}, {"name" : "Lars Thomsen / Kaj Kramer"}];
    }

  });