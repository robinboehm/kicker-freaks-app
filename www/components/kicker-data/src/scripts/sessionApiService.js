angular.module('robinboehm.kickerData')
  .factory('sessionApiService', function($http) {
    function create(credentials) {
      return $http.post('/api/sessions', credentials).then(function(sessionReponse) {
        return sessionReponse.data;
      });
    }

    return {
      create: create
    };
  });