angular.module('robinboehm.kickerMap')
  .controller('MapCtrl', function ($scope, $timeout, $stateParams, $ionicLoading, KickerData) {
    var locations;
    KickerData.getAll($scope.options)
      .then(function (responseLocations) {
        locations = responseLocations;
      })
      .then(onlyShowVisible);


    function onlyShowVisible() {
      if ($scope.map.control.getGMap && locations) {
        var currentBounds = $scope.map.control.getGMap().getBounds();
        $scope.map.markers = locations.filter(function (location) {
          return currentBounds.contains(new google.maps.LatLng(location.latitude, location.longitude));
        });
      }
    }

    $scope.map = {
      control: {},
      markerIcon: 'img/kicker-marker.png',
      center: {
        latitude: $stateParams.latitude || 51.456680,
        longitude: $stateParams.longitude || 7.009991
      },
      options: {scrollwheel: false},
      zoom: 16,
      models: [],
      events: {
        zoom_changed: onlyShowVisible,
        dragend: onlyShowVisible
      }
    }
  });