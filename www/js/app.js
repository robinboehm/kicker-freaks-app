angular.module('starter', [
  'ionic',
  'yaru22.angular-timeago',
  'robinboehm.kickerMap',
  'robinboehm.kickerForm',
  'robinboehm.kickerNear',
  'robinboehm.kickerLocation',
  'robinboehm.kickerTournament'
])

  .run(function ($ionicPlatform, $http) {

    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

      var deviceInformation = ionic.Platform.device();
      $http.defaults.headers.common['DeviceUUID'] = deviceInformation.uuid || 'TEST';
    });


  })

  .config(function ($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

      .state('signin', {
        url: '/sign-in',
        templateUrl: 'templates/sign-in.html'
        ,controller: 'SignInCtrl'
      })


      .state('forgotpassword', {
        url: '/forgot-password',
        templateUrl: 'templates/forgot-password.html'
      })

      // setup an abstract state for the tabs directive
      .state('tab', {
        url: "/tab",
        abstract: true,
        templateUrl: "templates/tabs.html"
      })

      // Each tab has its own nav history stack:

      .state('tab.map', {
        url: '/map/',
        views: {
          'tab-map': {
            templateUrl: 'components/kicker-map/src/templates/tab-map.html',
            controller: 'MapCtrl'
          }
        }
      })

      .state('tab.map-navigate', {
        url: '/map/navigate/:latitude/:longitude',
        views: {
          'tab-map': {
            templateUrl: 'components/kicker-map/src/templates/tab-map.html',
            controller: 'MapCtrl'
          }
        }
      })


      .state('tab.map-location', {
        url: '/map/location/:locationId',
        views: {
          'tab-map': {
            templateUrl: 'components/kicker-map/src/templates/map-location-show.html',
            controller: 'ShowLocationCtrl'
          }
        }
      })

      .state('tab.map-location-edit', {
        url: '/map/location/:locationId/edit',
        views: {
          'tab-map': {
            templateUrl: 'components/kicker-map/src/templates/map-location-edit.html',
            controller: 'KickerFormEditCtrl'
          }
        }
      })

      // Tournament
      .state('tab.tournament', {
        url: '/tournament',
        views: {
          'tab-tournament': {
            templateUrl: 'components/kicker-tournament/src/templates/tab-tournament.html'
          }
        }
      })

      .state('tab.tournament-players', {
        url: '/tournament/:tournamentId/players',
        views: {
          'tab-tournament': {
            templateUrl: 'components/kicker-tournament/src/templates/tab-tournament-players.html'
          }
        }
      })

      // NEAR
      .state('tab.near', {
        url: '/near',
        views: {
          'tab-near': {
            templateUrl: 'components/kicker-near/src/templates/tab-near.html',
            controller: 'NearCtrl'
          }
        }
      })

      .state('tab.near-location-new', {
        url: '/near/location/new',
        views: {
          'tab-near': {
            templateUrl: 'components/kicker-near/src/templates/near-location-new.html'
          }
        }
      })

      .state('tab.near-location', {
        url: '/near/location/:locationId',
        views: {
          'tab-near': {
            templateUrl: 'components/kicker-near/src/templates/near-location-show.html',
            controller: 'ShowLocationCtrl'
          }
        }
      })

      .state('tab.near-location-edit', {
        url: '/near/location/:locationId/edit',
        views: {
          'tab-near': {
            templateUrl: 'components/kicker-near/src/templates/near-location-edit.html',
            controller: 'KickerFormEditCtrl'
          }
        }
      });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/tab/near');

  });
